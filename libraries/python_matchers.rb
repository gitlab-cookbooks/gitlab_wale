if defined?(ChefSpec)
  ChefSpec.define_matcher :python_package

  # @example This is an example
  #   expect(chef_run).to install_python_package('foo')
  #
  # @param [String] resource_name
  #   the resource name
  #
  # @return [ChefSpec::Matchers::ResourceMatcher]
  #

  def install_python_package(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:python_package, :install, resource_name)
  end
  def remove_python_package(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:python_package, :remove, resource_name)
  end

  ChefSpec.define_matcher :python_virtualenv
  
  def create_python_virtualenv(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:python_virtualenv, :create, resource_name)
  end
  def delete_python_virtualenv(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:python_virtualenv, :delete, resource_name)
  end

  ChefSpec.define_matcher :python_runtime
  
  def install_python_runtime(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:python_runtime, :install, resource_name)
  end
  def remove_python_runtime(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:python_runtime, :remove, resource_name)
  end

end
