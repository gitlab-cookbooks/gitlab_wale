require "spec_helper"
require "chef-vault"

describe "gitlab_wale::default" do
  shared_examples 'installing wal-e' do
    it 'installs required packages' do
      expect(chef_run).to install_apt_package(['pv','daemontools','lzop'])
    end

    it 'installs packages required for poise' do
      expect(chef_run).to install_package('python-pip')
    end

    it 'installs python version 3' do
      expect(chef_run).to install_python_runtime('3')
    end

    it 'sets virtualenv directory' do
      expect(chef_run).to create_python_virtualenv('/opt/wal-e')
    end

    it 'installs python wal-e package' do
      expect(chef_run).to install_python_package('wal-e').with(version: '1.1.0')
    end

    it 'installs python google-compute-engine package' do
      expect(chef_run).to install_python_package('google-compute-engine')
    end

    it 'installs python google-cloud-storage package' do
      expect(chef_run).to install_python_package('google-cloud-storage')
    end

    it 'installs python gcloud package' do
      expect(chef_run).to install_python_package('gcloud')
    end

    it "creates the wal-e environment dir in the configured location" do
      expect(chef_run).to create_directory("/etc/wal-e.d/env").with(recursive: true)
    end

    it 'creates wal-e log directory' do
      expect(chef_run).to create_directory('/var/log/wal-e')
    end

    it 'creates a postgres user' do
      expect(chef_run).to create_user('postgres')
    end

    it 'creates GOOGLE_APPLICATION_CREDENTIALS file' do
      expect(chef_run).to create_file('/etc/wal-e.d/env/GOOGLE_APPLICATION_CREDENTIALS').with(
        content: '/etc/wal-e.d/gcs.json'
      )
    end

    it 'creates WALE_GS_PREFIX file' do
      expect(chef_run).to create_file('/etc/wal-e.d/env/WALE_GS_PREFIX')
    end

    it 'creates gcs.json file' do
      expect(chef_run).to create_file('/etc/wal-e.d/gcs.json').with(
        mode:    '0600',
        owner:   'postgres',
        content: 'gcs-json-credentials'
      )
    end

    it 'creates logrotate file' do
      expect(chef_run).to create_cookbook_file('/etc/logrotate.d/wal-e').with(
        mode: '0644',
        owner: 'root',
        group: 'root'
      )
    end

    it 'creates basebackup script file' do
      expect(chef_run).to create_template('/opt/wal-e/bin/backup.sh').with(
        mode: '0744',
        user: 'postgres'
      )
    end

    it 'creates full backup cron' do
      expect(chef_run).to create_cron('full wal-e backup').with(command: "/usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e backup-push /var/lib/postgresql/9.3/main > /tmp/wal-e_backup_push.log;")
    end

    it 'should not have tmpdir by default' do
      expect(chef_run).to_not create_file('/etc/wal-e.d/env/TMPDIR')
    end

    context 'GCS patch' do
      it 'patches upload.py' do
        expect(chef_run).to create_cookbook_file('/opt/wal-e/lib/python3.5/site-packages/wal_e/worker/upload.py').with(
          source: 'wale-uploader-with-gcs-patch.py',
          owner: 'root',
          group: 'root',
          mode: '0644'
        )
      end
    end

    context 'when tmpdir path is set, adds tmpdir env var' do
      normal_attributes['gitlab_wale']['tmpdir']['path'] = '/var/tmp/wale'

      it 'creates tmpdir env var' do
        expect(chef_run).to create_file('/etc/wal-e.d/env/TMPDIR').with(
          content: '/var/tmp/wale'
        )
      end

      context 'when tmpdir.create is default do not create the tmpdir' do
        it 'does not create tmpdir' do
          expect(chef_run).to_not create_directory('/var/tmp/wale')
        end
      end

      context 'when tmpdir.create is enabled, create the tmpdir' do
        normal_attributes['gitlab_wale']['tmpdir']['create'] = true
        it 'creates tmpdir' do
          expect(chef_run).to create_directory('/var/tmp/wale')
        end
      end
    end
  end

  context 'credentials in Chef vault' do
    normal_attributes['gitlab_wale']['credential_details']['chef_vault'] = 'gitlab_wale'

    before do
      allow(Chef::DataBag).to receive(:load).with('gitlab_wale').and_return({
        "_default_keys" => {"id" => "_default_keys" },
      })
      allow(ChefVault::Item).to receive(:load).with('gitlab_wale', '_default').and_return({
        "id" => "_default",
        "gitlab_wale" => {
          "credential_details" => {
              "google_application_credentials" => "gcs-json-credentials",
            }
          }
        })
    end

    it_behaves_like 'installing wal-e'
  end

  context 'credentials in a backend specified by attributes' do
    normal_attributes['gitlab_wale']['secrets'] = {
      'backend' => 'gkms',
      'path' => 'some-path',
      'key' => 'some-key'
     }

    before do
      expect_any_instance_of(Chef::Recipe).to receive(:get_secrets).with('gkms', 'some-path', 'some-key').and_return({
        'gitlab_wale' => {
          'credential_details' => {
            "google_application_credentials" => "gcs-json-credentials",
          }
        }
      })
    end

    it_behaves_like 'installing wal-e'
  end
end
