#
# Cookbook Name:: gitlab_wale
# Recipe:: default
#
# Copyright (c) 2017 Alex Hanselka, All Rights Reserved.

# Distribution pip needed for poise recipe below to work properly. The upstream
# pip that would be installed instead fails to install setuptools on ubuntu
# 16.04.

package 'python-pip' do
  case node['platform_version']
  when '16.04', '18.04'
    package_name 'python-pip'
  else
    package_name 'python3-pip'
  end
end

include_recipe 'poise-python'

creds =
  if (secrets_hash = node['gitlab_wale']['secrets']) && !secrets_hash.empty?
    get_secrets(secrets_hash['backend'],
                secrets_hash['path'],
                secrets_hash['key'])['gitlab_wale']['credential_details']
  else
    include_recipe 'gitlab-vault'
    GitLab::Vault.get(node, 'gitlab_wale', 'credential_details')
  end

apt_package ['pv','daemontools','lzop']

python_runtime node['gitlab_wale']['python_runtime_version'] do
  pip_version node['gitlab_wale']['pip_version']
end

python_virtualenv '/opt/wal-e' do
  pip_version node['gitlab_wale']['pip_version']
end

python_package 'wal-e' do
  version node['gitlab_wale']['wal-e_version']
end

python_package 'google-compute-engine'
python_package 'google-cloud-storage'
python_package 'gcloud'

user node['gitlab_wale']['postgres_user'] do
  manage_home true
end

file '/tmp/wal-e_backup_push.log' do
  owner node['gitlab_wale']['postgres_user']
  mode '0666'
  action :create_if_missing
end

directory '/var/log/wal-e' do
  owner node['gitlab_wale']['postgres_user']
  mode '0755'
end

cookbook_file '/etc/logrotate.d/wal-e' do
  action :create
  owner 'root'
  group 'root'
  mode '0644'
  source "wal-e.logrotate"
end

directory '/etc/wal-e.d/env' do
  recursive true
end

file '/etc/wal-e.d/env/WALE_GS_PREFIX' do
  content node['gitlab_wale']['gs_prefix']
end

file '/etc/wal-e.d/env/GOOGLE_APPLICATION_CREDENTIALS' do
  content '/etc/wal-e.d/gcs.json'
end

if node['gitlab_wale']['tmpdir']['path']
  file '/etc/wal-e.d/env/TMPDIR' do
    content node['gitlab_wale']['tmpdir']['path']
  end

  if node['gitlab_wale']['tmpdir']['create']
    directory node['gitlab_wale']['tmpdir']['path'] do
      owner node['gitlab_wale']['tmpdir']['owner']
      group node['gitlab_wale']['tmpdir']['group']
      mode node['gitlab_wale']['tmpdir']['mode']
      action :create
    end
  end
end

file '/etc/wal-e.d/gcs.json' do
  content creds['google_application_credentials']
  mode '0600'
  owner node['gitlab_wale']['postgres_user']
end

template '/opt/wal-e/bin/backup.sh' do
  source 'basebackup.sh.erb'
  mode '0744'
  owner node['gitlab_wale']['postgres_user']
end

cron 'full wal-e backup' do
  minute '0'
  hour '0'
  user node['gitlab_wale']['postgres_user']
  command node['gitlab_wale']['backup-cron']
end

if node['gitlab_wale']['wal-e_version'] == '1.1.0'
  cookbook_file '/opt/wal-e/lib/python3.5/site-packages/wal_e/worker/upload.py' do
    source 'wale-uploader-with-gcs-patch.py'
    owner 'root'
    group 'root'
    mode '0644'
  end
end
